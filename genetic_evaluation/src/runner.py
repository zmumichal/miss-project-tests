import cProfile
from math import cos
import math
from numba import cuda
import numpy


@cuda.jit
def rastrigin_on_device(v):
    genotype_number = cuda.grid(1)
    genes_count = v.shape[1]
    A = 10.

    res = A * genes_count
    for i in range(genes_count):
        xi = v[genotype_number, i]
        res += pow(xi, 2) - A * cos(2 * math.pi * xi)
    v[genotype_number, 0] = res


def get_rastrigin_fitness_on_cpu(v):
    ret = []
    for genotype_number in range(len(v)):
        genes_count = len(v[0])
        A = 10.

        res = A * genes_count
        for i in range(genes_count):
            xi = v[genotype_number, i]
            res += pow(xi, 2) - A * cos(2 * math.pi * xi)
        ret.append(res)
    return numpy.array(ret)


def get_rastrigin_fitness(genotype_rows):
    genotypes_count = len(genotype_rows[:])

    blocks_per_grid = genotypes_count
    threads_per_block = 1

    rastrigin_on_device[blocks_per_grid, threads_per_block](genotype_rows)
    return genotype_rows[:, 0]


def mutate_genotypes(A, gaussian_scale):
    genotypes_count = len(A)
    genes_count = len(A[0])

    for genotype in range(genotypes_count):
        for gene in range(genes_count):
            A[genotype, gene] = min(max(numpy.random.normal(A[genotype, gene], gaussian_scale), -5.12), 5.12)

    return A


def replace_worst_with_best(fitness, population):
    min_index = fitness.argmin()
    max_ndex = fitness.argmax()
    population[max_ndex] = population[min_index]


def main(iterations=256, genes=512, genotypes_count=32, gaussian_mutation_scale=0.1):
    initial_population = numpy.random.uniform(low=-5.12, high=5.12, size=(genotypes_count, genes))
    population = numpy.copy(initial_population)
    for i in range(iterations):
        print "step ", i, " of ", iterations
        fitness = get_rastrigin_fitness(numpy.copy(population))
        # fitness = get_rastrigin_fitness_on_cpu(population)

        replace_worst_with_best(fitness, population)

        mutate_genotypes(population, gaussian_mutation_scale)

    print get_rastrigin_fitness(numpy.copy(initial_population))
    print get_rastrigin_fitness(numpy.copy(population))


cProfile.run('main()', sort='cumtime')
