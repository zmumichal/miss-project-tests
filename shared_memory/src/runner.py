import time
import math
import numpy as np
from matrix_device import matmul
from matrix_shared import fast_matmul, TPB
from matrix_gu import gu_matmul


def main():
    n = TPB * 32

    A = np.random.rand(n, n)
    B = np.random.rand(n, n)

    threadsperblock = (TPB, TPB)
    blockspergrid_x = int(math.ceil(float(n) / threadsperblock[0]))
    blockspergrid_y = int(math.ceil(float(n) / threadsperblock[1]))
    blockspergrid = (blockspergrid_x, blockspergrid_y)

    C = np.zeros((n, n))
    now = time.time()
    matmul[blockspergrid, threadsperblock](A, B, C)
    print 'ordinary:\t\t', time.time() - now

    D = np.zeros((n, n))
    now = time.time()
    fast_matmul[blockspergrid, threadsperblock](A, B, D)
    print 'using shared:\t', time.time() - now

    # UNUSABLE - takes to long
    # E = np.zeros((n, n))
    # now = time.time()
    # gu_matmul(A, B, E)
    # print 'guvectorize:\t', time.time() - now


main()

#           GF 730m
#
# ordinary:		1.65595889091
# using shared:	0.755923986435
# guvectorize:	8.44530296326 (on cpu; on gpu gets timeout)