from numba import cuda, export
import numpy as np


@cuda.jit
def f1(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f2(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f3(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f4(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f5(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f6(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f7(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f8(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f9(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f10(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f11(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f12(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f13(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f14(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f15(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


@cuda.jit
def f16(a, b, result):
    i = cuda.grid(1)
    result[i] = a[i] + b[i]


A = np.random.rand(2)
B = np.random.rand(2)
C = np.random.rand(2)

f1(A, B, C)
f2(A, B, C)
f3(A, B, C)
f4(A, B, C)
f5(A, B, C)
f6(A, B, C)
f7(A, B, C)
f8(A, B, C)
f9(A, B, C)
f10(A, B, C)
f11(A, B, C)
f12(A, B, C)
f13(A, B, C)
f14(A, B, C)
f15(A, B, C)
f16(A, B, C)

export('f1 f4(f4, f4)')(f1)
export('f2 f4(f4, f4)')(f2)
export('f3 f4(f4, f4)')(f3)
export('f4 f4(f4, f4)')(f4)
export('f5 f4(f4, f4)')(f5)
export('f6 f4(f4, f4)')(f6)
export('f7 f4(f4, f4)')(f7)
export('f8 f4(f4, f4)')(f8)
export('f9 f4(f4, f4)')(f9)
export('f10 f4(f4, f4)')(f10)
export('f11 f4(f4, f4)')(f11)
export('f12 f4(f4, f4)')(f12)
export('f13 f4(f4, f4)')(f13)
export('f14 f4(f4, f4)')(f14)
export('f15 f4(f4, f4)')(f15)
export('f16 f4(f4, f4)')(f16)