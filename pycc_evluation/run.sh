#!/bin/bash
# adjust interpreter path
export PATH="/home/zmumichal/anaconda/bin:$PATH"

INTERPRETER="/home/zmumichal/anaconda/bin/python"
PYCC="/home/zmumichal/anaconda/bin/pycc"

rm *.pyc
echo "No PYC"
$INTERPRETER -m cProfile runner.py | grep compiler.py:454

echo "PYC"
$PYCC runner.py
$INTERPRETER -m cProfile runner.py | grep compiler.py:454
