from math import sin
from numbapro import vectorize
from numpy import arange
import cProfile

# makes no sense - only shows it works

@vectorize(['float32(float32, float32)'], target='gpu')  #set to cpu to see speedup
def add2(a, b):
    result = sin(a) + sin(b)
    for i in range(0, 100000):
        result = sin(result)
    return result


X = arange(10000, dtype='float32')
Y = X * 2
cProfile.run("print add2(X, Y)")