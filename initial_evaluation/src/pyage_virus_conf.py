from pyage.core import address
from pyage.core.agent.agent import unnamed_agents
from pyage.core.agent.aggregate import AggregateAgent
from pyage.core.emas import EmasService
from pyage.core.locator import TorusLocator
from pyage.core.migration import ParentMigration
from pyage.core.stats.gnuplot import StepStatistics
from pyage.core.stop_condition import StepLimitStopCondition

from virus.crossover import SinglePointVirusCrossover
from virus.evaluation import VirusRosenbrockEvaluation
from virus.initializer import virus_emas_initializer
from virus.mutation import UniformVirusMutation


agents_count = 5
agents = unnamed_agents(agents_count, AggregateAgent)

stop_condition = lambda: StepLimitStopCondition(1000)

aggregated_agents = lambda: virus_emas_initializer(40, energy=100, size=50, lowerbound=-10, upperbound=10)

emas = EmasService

minimal_energy = lambda: 0
reproduction_minimum = lambda: 90
migration_minimum = lambda: 120
newborn_energy = lambda: 100
transferred_energy = lambda: 40

evaluation = VirusRosenbrockEvaluation
crossover = SinglePointVirusCrossover
mutation = lambda: UniformVirusMutation(probability=1, radius=1)

address_provider = address.SequenceAddressProvider

migration = ParentMigration
locator = TorusLocator


stats = StepStatistics