import numpy as np

# see http://www.lcc.uma.es/~ccottap/papers/labsASC.pdf

# implemented in no-python to make cuda migrtion easier
# (these functions are intended to become subprocedures)


def initialize_helper_tables(genotype, products, sums):
    genes_count = len(products) + 1
    for distance in range(1, genes_count):
        for index in range(0, genes_count - distance):
            row, col = distance - 1, index
            products[row][col] = genotype[index] * genotype[index + distance]
            sums[row] += products[row][col]


def get_merit_factor_if_flipped(flipped_gene_index, products, sums):
    genes_count = len(sums) + 1

    sum = 0
    for p in range(0, genes_count - 1):
        tmp = sums[p]

        # vertical
        if p <= genes_count - 2 - flipped_gene_index:
            tmp -= 2 * products[p][flipped_gene_index]

        # diagonal
        if p < flipped_gene_index:
            tmp -= 2 * products[p][flipped_gene_index - 1 - p]

        sum += tmp

    return genes_count * genes_count / 2. / sum


def update_helper_tables(flipped_gene_index, products, sums):
    genes_count = len(products) + 1

    for p in range(0, genes_count - 1):
        # vertical
        if p <= genes_count - 2 - flipped_gene_index:
            sums[p] -= 2 * products[p][flipped_gene_index]
            products[p][flipped_gene_index] *= -1

        # diagonal
        if p < flipped_gene_index:
            sums[p] -= 2 * products[p][flipped_gene_index - 1 - p]
            products[p][flipped_gene_index - 1 - p] *= -1


def get_merit_factor(sums):
    genes_count = len(sums) + 1

    sum = 0
    for i in range(len(sums)):
        sum += sums[i]

    return genes_count * genes_count / 2. / sum


def get_empty_helper_tables(genes_count):
    products = np.zeros((genes_count - 1, genes_count - 1))
    sums = np.zeros(genes_count - 1)
    return products, sums

