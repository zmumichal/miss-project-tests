from pprint import pprint
import numpy as np
from helper_tables_manipulation import get_empty_helper_tables, initialize_helper_tables, update_helper_tables, \
    get_merit_factor_if_flipped, get_merit_factor


def get_random_population(genes_count, genotypes_count):
    return np.random.choice([-1, 1], (genotypes_count, genes_count))


population = get_random_population(5, 3)

for genotype in population:
    products, sums = get_empty_helper_tables(len(genotype))
    initialize_helper_tables(genotype, products, sums)

    print "original: ", genotype
    print "with factor: ", get_merit_factor(sums)

    neighborhood = [get_merit_factor_if_flipped(i, products, sums) for i in range(len(genotype))]
    gene_giving_best_factor = neighborhood.index(max(neighborhood))
    update_helper_tables(gene_giving_best_factor, products, sums)

    print "new factor: ", max(neighborhood)



